/**
 * Tehtävä: Urheilu 1

Kehitystehtävänä on määritellä olio-ohjelmointikielille ominainen luokkamäärittely ja periytyminen JavaScript-kielellä.

Määrittele yliluokka Henkilo, joka sisältää ihmisen henkilötietoja:

etunimet,
sukunimi,
kutsumanimi,
syntymävuosi

Määrittele luokka Urheilija, joka perii Henkilo-luokan ja toteuttaa lisäksi saantifunktiot 
(get- ja set-) 
Urheilija-luokalle merkityksellisiin attribuutteihin. 
Lisää Urheilija luokkaan seuraavat ominaisuudet:

linkki kuvaan,
omapaino,
laji,
saavutukset.
Kirjoita nämä vaatimukset toteuttava koodi joka toimii node.js-tulkissa.

Toteuta koodi. Lisää koodiin esimerkkejä Urheilija –olioista.
Talleta toteutuksesi gitlab (tai github) -ympäristöön omaan projektiisi hakemistoon: Urheilu1
Kirjoita tehtävästä kommentointi ja huomioita kehittäjän blogiin (hackmd.io).
 */

/**
 * Yliluokka Henkilo
 */
class Henkilo {
  constructor(etunimi, sukunimi, kutsumanimi, syntymävuosi) {
    this.etunimi = etunimi;
    this.sukunimi = sukunimi;
    this.kutsumanimi = kutsumanimi;
    this.syntymävuosi = syntymävuosi;
  }
}

/**
 * Urheilija luokka, joka perii Henkilo-luokan
 */
class Urheilija extends Henkilo {
  constructor(
    etunimi,
    sukunimi,
    kutsumanimi,
    syntymävuosi,
    linkkiKuvaan,
    omapaino,
    laji,
    saavutukset
  ) {
    super(etunimi, sukunimi, kutsumanimi, syntymävuosi);
    this.linkkiKuvaan = linkkiKuvaan;
    this.omapaino = omapaino;
    this.laji = laji;
    this.saavutukset = saavutukset;
  }

  //Getter

  /**
   * Get urheilijan etunimi
   * @returns {etunimi} - Urheilijan etunimi
   */
  get Etunimi() {
    return this.etunimi;
  }

  /**
   * Get urheilijan sukunimi
   * @returns {sukunimi} - Urheilijan etunimi
   */
  get Sukunimi() {
    return this.sukunimi;
  }
  /**
   * Get urheilijan sukunimi
   *  @returns {kutsumanimi} - Urheilijan kutsumanimi
   */

  get Kutsumanimi() {
    return this.kutsumanimi;
  }

  /**
   * Get urheilijan syntymävuosi
   *  @returns {syntymavuosi} - Urheilijan syntymävuosi
   */
  get Syntymävuosi() {
    return this.syntymävuosi;
  }

  /**
   * Get urheilijan linkkiKuvaan
   *  @returns {linkkiKuvaan} - Urheilijan linkkiKuvaan
   */
  get LinkkiKuvaan() {
    return this.linkkiKuvaan;
  }

  /**
   * Get urheilijan omapaino
   *  @returns {linkkiKuvaan} - Urheilijan omapaino
   */
  get Omapaino() {
    return this.omapaino;
  }

  /**
   * Get urheilijan laji
   *  @returns {laji} - Urheilijan laji
   */
  get Laji() {
    return this.laji;
  }

  /**
   * Get urheilijan saavutukset
   *  @returns {saavutukset} - Urheilijan saavutukset
   */
  get Saavutukset() {
    return this.saavutukset;
  }

  //Setter

  /**
   * Set urhelijan etunimi
   * @param {etunimi} - urheilijan etunimi
   */
  set Etunimi(etunimi) {
    this.etunimi = etunimi;
  }

  /**
   * Set urhelijan sukunimi
   * @param {etunimi} - urheilijan sukunimi
   */
  set Sukunimi(sukunimi) {
    this.sukunimi = sukunimi;
  }

  /**
   * Set urhelijan kutsumanimi
   * @param {etunimi} - urheilijan sukunimi
   */
  set Kutsumanimi(kutsumanimi) {
    this.kutsumanimi = kutsumanimi;
  }

  /**
   * Set urhelijan syntymavuosi
   * @param {syntymavuosi} - urheilijan syntymävuosi
   */
  set Syntymävuosi(syntymavuosi) {
    this.syntymävuosi = syntymavuosi;
  }

  /**
   * Set urhelijan linkki kuvaan
   * @param {linkkiKuvaan} - urheilijan linkkiKuvaan
   */
  set LinkkiKuvaan(linkkiKuvaan) {
    this.linkkiKuvaan = linkkiKuvaan;
  }

  /**
   * Set urhelijan omapaino
   * @param {omapaino} - urheilijan omapaino
   */
  set Omapaino(omapaino) {
    this.omapaino = omapaino;
  }

  /**
   * Set urhelijan  laji
   * @param {laji} - urheilijan  laji
   */
  set Laji(laji) {
    this.laji = laji;
  }

  /**
   * Set urhelijan saavutukset
   * @param {(saavutukset} - urheilijan saavutukset
   */
  set Saavutukset(saavutukset) {
    this.saavutukset = this.saavutukset;
  }

  /**
   * Urheilija kaikki tiedot
   * @returns {etunimi, sukunimi, kutsumanimi, syntymavuosi
   * ,linkkiKuvaan, omapaino, laji, saavutukset}
   */
  toString() {
    return (
      "[" +
      this.etunimi +
      " " +
      this.sukunimi +
      " " +
      this.kutsumanimi +
      " " +
      this.syntymävuosi +
      " " +
      this.linkkiKuvaan +
      " " +
      this.omapaino +
      " " +
      this.laji +
      " " +
      this.saavutukset +
      "]"
    );
  }
}

// Urheilija 1
const urheilija1 = new Urheilija(
  "Henri",
  "Savolainen",
  "Ilari",
  1989,
  null,
  78,
  "juoksu",
  "kulta"
);

console.log(urheilija1.toString());

//Urheilija 2
const urheilija2 = new Urheilija();
urheilija2.Etunimi = "Juha";
urheilija2.Sukunimi = "Mononen";
urheilija2.laji = "Avantouinti";

console.log(urheilija2.toString());

//Urhelija 3
const urheilija3 = new Urheilija(
  "Lauri",
  "Peiponen",
  "Lauri",
  1991,
  null,
  70,
  "ampumahiihto",
  "bronsi"
);

console.log(urheilija3.toString());
