/**
 * Tehtävä 4a: Luo REST API sanakirjaa varten.
 * Tekijä: Hoang Tran - LTDNS20 
 * Opisnr: 2108219 
 */

//Array to save words
let dictionary = [ ];
const express = require("express");

var app = express();

const fs = require('fs');
app.use(express.json()); 
app.use(express.urlencoded({ extended: true }));

//Read Data from file sanakirja
const data = fs.readFileSync('./sanakirja.txt',
  {encoding: 'utf8',flag: 'r'});
   
  //spilit words from line
  const splitLines = data.split(/\r?\n/);
  //Adding object fin, eng to array dictionary 
  splitLines.forEach((line) => {
    const words = line.split(" "); //sanat taulukkoon words
    console.log(words);
    const word = {
        fin:words[0],
        eng:words[1]
    };
    dictionary.push(word);
    console.log(dictionary);

  }) 

  //Convert array to Map
  var dictionary_Map = new Map(dictionary.map(i => [i.fin, i.eng]));
    console.log(dictionary_Map);

// GET all dictionary
app.get("/words", (req, res) => { 
   res.json(dictionary);
});

// Search finnish - english
    app.get("/words/:fin", (req,res) =>{  
      var searchWord = String(req.params.fin);
      res.json(dictionary_Map.get(searchWord));
    });

// ADD a word
//Write Data to file sanakirja

app.post("/words", (req, res) => {
  let word = JSON.stringify(req.body);
  let obj = JSON.parse(word);
  let newWord = ("\n" + obj.fin + " " + obj.eng);
  
try {
  //Write file from postman
  fs.writeFileSync("sanakirja.txt", newWord ,{flag:'a+'});   
  console.log("File written successfully");
} catch(err) {
  console.error(err);
}

//Readfile to check new words
console.log(fs.readFileSync("sanakirja.txt", "utf8")); 
  res.json(dictionary);

});

app.listen(3000, () => {
  console.log("Server listening at port 3000");
});
